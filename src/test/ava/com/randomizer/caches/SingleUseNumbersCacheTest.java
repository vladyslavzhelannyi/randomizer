package test.ava.com.randomizer.caches;

import main.java.com.randomizer.caches.SingleUseNumbersCache;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


public class SingleUseNumbersCacheTest {
    SingleUseNumbersCache cut = new SingleUseNumbersCache(1, 10);

    static Arguments[] getCacheSizeTestArgs(){
        return new Arguments[]{
          Arguments.arguments(new int[]{ 3, 4, 5, 6, 7, 8, 9}, new int[]{3, 4, 5, 7, 9}, 5),
          Arguments.arguments(new int[]{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}, new int[]{}, 12),
        };
    }

    static Arguments[] getNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3, new int[]{2, 3, 4, 5, 6, 7, 8}, new int[]{2, 4, 5, 6, 8}, 6),
                Arguments.arguments(5, new int[]{2, 3, 4, 5, 6, 7, 8}, new int[]{}, 7),
        };
    }

    @ParameterizedTest
    @MethodSource("getCacheSizeTestArgs")
    void getCacheSizeTest(int[] selectedRangeArg, int[] notUsedNumbersArg, int expected) {
        cut.selectedRange = selectedRangeArg;
        cut.notUsedNumbers = notUsedNumbersArg;
        int actual = cut.getCacheSize();
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("getNumberTestArgs")
    void getNumberTest(int indexArg, int[] selectedRangeArg, int[] notUsedNumbersArg, int expected) {
        cut.selectedRange = selectedRangeArg;
        cut.notUsedNumbers = notUsedNumbersArg;
        int actual = cut.getNumber(indexArg);
        Assertions.assertEquals(expected, actual);
    }
}
