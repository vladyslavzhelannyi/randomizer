package test.ava.com.randomizer.services;

import main.java.com.randomizer.caches.SingleUseNumbersCache;
import main.java.com.randomizer.services.Randomizer;
import main.java.com.randomizer.wrappers.CacheWrapper;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.FileNotFoundException;
import java.net.CacheRequest;
import java.util.Random;
import java.util.Scanner;

public class RandomizerTest {
    private final Random random = Mockito.mock(Random.class);
    private final Scanner scanner = Mockito.mock(Scanner.class);
    private final SingleUseNumbersCache singleUseNumbersCache = Mockito.mock(SingleUseNumbersCache.class);
    private final CacheWrapper cacheWrapper = Mockito.mock(CacheWrapper.class);

    Randomizer cut = new Randomizer(random, scanner, cacheWrapper);

    static Arguments[] runTestArgs(){
        return new Arguments[]{
          Arguments.arguments(3 , 11, "generate", "exit", "no", "exit", "yes", 9, 3, 2, 1),
          Arguments.arguments(23 , 23, "generate", "exit", "no", "exit", "yes", 1, 0, 2, 1),
        };
    }

    @ParameterizedTest
    @MethodSource("runTestArgs")
    void runTest(int min, int max, String input1, String input2, String input3, String input4, String input5,
                 int cacheSize, int randomNum, int getSizeTimes, int getNumberTimes1) throws FileNotFoundException {
        Mockito.when(scanner.nextInt()).thenReturn(min).thenReturn(max);
        Mockito.when(cacheWrapper.getCache(min, max)).thenReturn(singleUseNumbersCache);
        Mockito.when(scanner.next()).thenReturn(input1).thenReturn(input2).thenReturn(input3).thenReturn(input4).
                thenReturn(input5);
        Mockito.when(singleUseNumbersCache.getCacheSize()).thenReturn(cacheSize);
        Mockito.when(random.nextInt(singleUseNumbersCache.getCacheSize()))
                .thenReturn(randomNum);

        cut.run();

        Mockito.verify(singleUseNumbersCache, Mockito.times(getSizeTimes)).getCacheSize();
        Mockito.verify(singleUseNumbersCache, Mockito.times(getNumberTimes1)).getNumber(randomNum);
    }
}
