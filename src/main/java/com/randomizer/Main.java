package main.java.com.randomizer;

import main.java.com.randomizer.services.Randomizer;
import main.java.com.randomizer.wrappers.CacheWrapper;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        CacheWrapper cacheWrapper = new CacheWrapper();
        Randomizer randomizer = new Randomizer(random, scanner, cacheWrapper);
        randomizer.run();
    }
}
