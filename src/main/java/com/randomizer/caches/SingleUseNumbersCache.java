package main.java.com.randomizer.caches;

public class SingleUseNumbersCache {
    private int min;
    private int max;
    public int[] selectedRange; //паблик для тестов без использования рефлексии
    public int[] notUsedNumbers; //паблик для тестов без использования рефлексии
    private int cacheSize;

    public SingleUseNumbersCache(int min, int max) {
        this.min = min;
        this.max = max;
        fillArray();
    }

    public int getCacheSize() {
        checkIfCacheEmpty();
        return cacheSize;
    }

    public int getNumber(int index){
        checkIfCacheEmpty();
        int numberToSend = notUsedNumbers[index];
        int newArrayLength = cacheSize - 1;
        int[] tempArray = new int[newArrayLength];
        int newArrayIndex = 0;
        for (int i = 0; i < cacheSize; i++) {
            if (i != index) {
                tempArray[newArrayIndex] = notUsedNumbers[i];
                newArrayIndex++;
            }
        }
        notUsedNumbers = tempArray;
        return numberToSend;
    }

    private void fillArray(){
        //Заполняем массив числами в диапазоне
        int arrayLength = max - min + 1;
        selectedRange = new int[arrayLength];
        int valueToFill = min;
        for (int i = 0; i < arrayLength; i++) {
            selectedRange[i] = min;
            min++;
        }
        notUsedNumbers = selectedRange;
        cacheSize = notUsedNumbers.length;
    }

    private void checkIfCacheEmpty(){
        cacheSize = notUsedNumbers.length;
        if(cacheSize == 0){
            notUsedNumbers = selectedRange;
            cacheSize = notUsedNumbers.length;
        }
    }
}
