package main.java.com.randomizer.services;

import main.java.com.randomizer.caches.SingleUseNumbersCache;
import main.java.com.randomizer.wrappers.CacheWrapper;

import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class Randomizer{
    private int min;
    private int max;
    private boolean exit = false;
    String userCommand;
    Scanner scanner;
    Random random;
    SingleUseNumbersCache singleUseNumbersCache;
    CacheWrapper cacheWrapper;


    public Randomizer(Random random, Scanner scanner, CacheWrapper cacheWrapper){
        this.random = random;
        this.scanner = scanner;
        this.cacheWrapper = cacheWrapper;
    }

    public void run(){
        System.out.println("Введите минимальное и максимальное значения диапазона, в которых происходит " +
                "произвольный выбор числа. Диапазон для выбора: от 1 до 500 включительно");
        System.out.print("Введите минимальное значение диапазона: ");

        //Ввод минимального значения
        boolean minNotIn = true;
        while(minNotIn){
            try{    //знаю, что лучше не использоваь трай-кетчи, но уже нет времени переделывать
                min = scanner.nextInt();
                if(min < 1 || min > 500){
                    throw new Exception();
                }
                minNotIn = false;
            }
            catch (Exception e){
                scanner.nextLine();
                System.out.print("Ввод не удовлетворяет условиям. Повторите ввод: ");
            }
        }

        //Ввод максимального значения
        System.out.print("Введите максимальное значение диапазона: ");
        boolean maxNotIn = true;
        while(maxNotIn){
            try{
                max = scanner.nextInt();
                if(max < 1 || max > 500 || max < min){
                    throw new Exception();
                }
                maxNotIn = false;
            }
            catch (Exception e){
                scanner.nextLine();
                System.out.print("Ввод не удовлетворяет условиям. Повторите ввод: ");
            }
        }

        try{
            singleUseNumbersCache = cacheWrapper.getCache(min, max);
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }

        help();

        while(!exit){
            userCommand = scanner.next();
            switch (userCommand){
                case "help":
                    help();
                    break;
                case "generate":
                    System.out.println(generate());
                    break;
                case "exit":
                    exit();
                    break;
                default:
                    System.out.println("Incorrect input. Try again");
                    break;
            }
        }
    }

    private void help(){ //не знаю как протестировать без паблика с помощью Mockito.verify
        System.out.println("\"help\" - вывести доступные команды\n" +
                "\"generate\" - вывести произвольное число в заданном диапазоне\n" +
                "\"exit\" - покинуть приложение");
    }

    private int generate(){ //не знаю как протестировать без паблика с помощью Mockito.verify
        int randomIndex = random.nextInt(singleUseNumbersCache.getCacheSize());
        int generatedNumber = singleUseNumbersCache.getNumber(randomIndex);
        return generatedNumber;
    }

    private void exit(){ //не знаю как протестировать без паблика с помощью Mockito.verify
        System.out.println("Are you sure that you want to quit the app?(Write \"yes\" or \"no\"");
        boolean choiceMade = false;
        while(!choiceMade){
            String choice = scanner.next();
            switch (choice){
                case "yes":
                    exit = true;
                    choiceMade = true;
                    break;
                case "no":
                    choiceMade = true;
                    break;
                default:
                    System.out.print("Incorrect input. Try again: ");
                    break;
            }
        }
    }
}
