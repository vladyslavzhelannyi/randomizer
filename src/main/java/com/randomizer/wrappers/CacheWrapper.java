package main.java.com.randomizer.wrappers;

import main.java.com.randomizer.caches.SingleUseNumbersCache;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class CacheWrapper {
    public SingleUseNumbersCache getCache(int min, int max) throws FileNotFoundException {
        return new SingleUseNumbersCache(min, max);
    }
}
